from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^test$', views.built_in_aes, name='test'),
    url(r'^encrypt', views.encrypt, name='encrypt'),
    url(r'^webencrypt', views.webencrypt, name='encrypt'),
    url(r'^webdecrypt', views.webdecrypt, name='encrypt'),
    url(r'^generate', views.generate, name='encrypt'),
    url(r'^decrypt', views.decrypt, name='decrypt'),
    url(r'^encryptcbc', views.encryptCBC, name='encryptcbc'),
    url(r'^decryptcbc', views.decryptCBC, name='decryptcbc'),
]
